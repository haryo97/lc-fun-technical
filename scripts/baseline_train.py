"""
Script to train a baseline model.

I created this script before I put it into main module
"""
# pylint: disable=all


import sys

sys.path.append(".")


from typing import Any, Dict, List

import rich
from lc_fun_technical.factory import MAPPER_COMPOSE_FUNC
from lc_fun_technical.modeling.lit_core import (
    LitAudioClassifier,
    LitAudioClassifierArgs,
)
from lc_fun_technical.pl_dataprocesser import SpeechCommandLitDataModule
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from pytorch_lightning.loggers import WandbLogger
import pytorch_lightning as pl
from rich.console import Console


console = Console()


config_dictionary = {
    "seed": 1234,
    "data": {
        "sample_rate": 16000,
        "lit_data_args": {
            "root_folder_raw": "data/raw",
            "transform_train": "default_cps",
            "batch_size_train": 256,
            "batch_size_val": 256,
            "batch_size_test": 256,
        },
    },
    "modeling": {
        "learning_rate": 0.001,
        "backbone_choice": "resnet34",
        "num_classes": 12,
        "pretrained": False,
    },
    "training_process_args": {
        "early_stopping_args": {
            "monitor": "valid_loss",
            "patience": 5,
            "mode": "min",
        },
        "model_checkpoint_args": {
            "dirpath": "outputs/model/baseline/",
            "filename": "{epoch}-{valid_loss:.4f}-{valid_acc:.2f}",
            "monitor": "valid_loss",
            "save_last": True,
            "save_top_k": 1,
            "every_n_epochs": 1,
        },
        "wandb_logger_args": {"name": "baseline", "project": "lcspeech"},
        "trainer_args": {
            "gpus": [0, 1],
            "deterministic": True,
            "precision": 16,
            "strategy": "ddp",
            "gradient_clip_val": 1.0,
            "max_epochs": 1000,
        },
    },
}


def produce_lit_datamodule(data_config: Dict[str, Any]) -> SpeechCommandLitDataModule:
    """
    Lit datamodule wrangling goes here!
    """
    rich.print(f"Producing Lit Data Module with {data_config}")
    lit_datamodule_args = data_config.get("lit_data_args", {})
    sample_rate = data_config.get("sample_rate", 16000)

    for key in ["transform_train", "transform_valid", "transform_test"]:
        if key in lit_datamodule_args:
            lit_datamodule_args[key] = MAPPER_COMPOSE_FUNC[lit_datamodule_args[key]](
                sample_rate=sample_rate
            )

    lit_datamodule = SpeechCommandLitDataModule(**lit_datamodule_args)
    return lit_datamodule


def produce_lit_model(modeling_config: Dict[str, Any]) -> LitAudioClassifier:
    """
    Produce lit model here!
    """
    lit_args = LitAudioClassifierArgs(**modeling_config)
    lit_model = LitAudioClassifier(lit_args)
    return lit_model


def prepare_and_run_training(
    training_process_cfg: Dict[str, Any],
    lit_datamodule: SpeechCommandLitDataModule,
    lit_model: LitAudioClassifier,
) -> None:
    """
    Prepare callback, loggers,
    """
    early_stopping_args = training_process_cfg.get("early_stopping_args")
    model_checkpoint_args = training_process_cfg.get("model_checkpoint_args")
    wandb_logger_args = training_process_cfg.get("wandb_logger_args")
    callbacks: List[Any] = []
    trainer_args: Dict[str, Any] = training_process_cfg.get(  # type: ignore
        "trainer_args"
    )
    logger = None
    if early_stopping_args is not None:
        callbacks.append(EarlyStopping(**early_stopping_args))
    if model_checkpoint_args is not None:
        callbacks.append(ModelCheckpoint(**model_checkpoint_args))
    if wandb_logger_args is not None:
        logger = WandbLogger(name="baseline", project="lcspeech")
    trainer = Trainer(callbacks=callbacks, logger=logger, **trainer_args)
    trainer.fit(lit_model, datamodule=lit_datamodule)
    trainer.test(lit_model, lit_datamodule, ckpt_path="best")


def run_experiment() -> None:
    """
    EXPERIMENT GO BRRR
    """
    pl.seed_everything(config_dictionary.get("seed"), workers=True)  # type: ignore
    data_config = config_dictionary.get("data")
    modeling_config = config_dictionary.get("modeling")
    training_process_cfg = config_dictionary.get("training_process_args")
    lit_datamodule = produce_lit_datamodule(data_config)  # type: ignore
    lit_model = produce_lit_model(modeling_config)  # type: ignore
    prepare_and_run_training(
        training_process_cfg, lit_datamodule, lit_model  # type: ignore
    )


if __name__ == "__main__":
    run_experiment()
