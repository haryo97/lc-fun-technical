"""
Script to run experiment from scratch
"""
import sys
import os

import hydra
from hydra.utils import get_original_cwd
from omegaconf import OmegaConf
from rich.console import Console

from lc_fun_technical.experiment.core import MainExperiment, Wav2VecExperiment


@hydra.main(config_path="experiment_config", config_name="main")
def my_app(cfg) -> None:  # type: ignore
    """MAIN APP FOR RUNNING EXPERIMENT!"""
    os.chdir(get_original_cwd())
    sys.path.append(".")

    console = Console()
    console.print(OmegaConf.to_yaml(cfg))
    config_dicts = OmegaConf.to_object(cfg)
    gpus = config_dicts["gpu"]  # type: ignore
    experiment_type = config_dicts["experiment_type"]  # type: ignore
    test_config = config_dicts["test_config"]  # type: ignore
    if not test_config:
        input("Press Enter if you're sure to run your experiment")
        if experiment_type == "main":
            exp = MainExperiment(config_dicts.get("main_config"), gpus)  # type: ignore
            exp.run()
        elif experiment_type == "wav2vec":
            exp = Wav2VecExperiment(config_dicts.get("main_config"), gpus)  # type: ignore
            exp.run()
        else:
            raise NotImplementedError(f"{experiment_type} is not implemented")


if __name__ == "__main__":
    my_app()  # pylint: disable=all
