# lc-fun-technical

Installation:

1. Download Pytorch (pip or conda install [https://pytorch.org/](here)). Install torchvision, cudatoolkit, and torchaudio too.
2. Install `PySoundFile` if you're using Windows or `sox` if you're using Linux using `pip`
3. Do `pip install -r requirements.txt`

For developer:
1. `pip install -r requirements-dev.txt`
2. `pre-commit install` to check your commit before you push to origin.

## How to train

do:

```
python run_experiment.py gpu=<GPU> main_config=<SCENARIO> experiment_type=<EXPERIMENT TYPE> hydra.run.dir=playground test_config=false
```

Explanation
- `<GPU>`: Gpus used in List of integer. e.g.: `[0]` or `[0,1]` (gpu id in `nvidia-smi`)
- `<SCENARIO>`: scenario experiment that you want to use. See `experiment_config/main_config` for the list of experiment. See `recap_experiment` notebook
- `<EXPERIMENT TYPE>`: experiment pipeline type. choices are: `main` (resnet and resnext model) and `wav2vec`: (wav2vec2 model)

Example:

```
python run_experiment.py gpu=[0] main_config=wav2vec_aug_nois experiment_type=wav2vec hydra.run.dir=playground test_config=false
```

Press ENTER after being prompted to make sure the config is true.

## Testing

```
pytest -v tests
```
