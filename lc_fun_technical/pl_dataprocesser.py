"""
Module to process data to be input of PYtorch Lightning
(make it easier to process)
"""

from pathlib import Path
from typing import Any, Callable, Optional

import numpy as np
from numpy.typing import NDArray
import pytorch_lightning as pl
import torch
from sklearn.utils.class_weight import compute_class_weight
from torch.utils.data import DataLoader

from .data import SpeechCommandsDataset


class SpeechCommandLitDataModule(  # pylint: disable=too-many-instance-attributes
    pl.LightningDataModule
):
    """
    Lit data module
    """

    def __init__(  # pylint: disable=too-many-arguments
        self,
        root_folder_raw: str,
        transform_train: Callable[[str], torch.Tensor],
        transform_valid: Optional[Callable[[str], torch.Tensor]] = None,
        transform_test: Optional[Callable[[str], torch.Tensor]] = None,
        speech_command_version: str = "speech_commands_v0.02",
        batch_size_train: int = 32,
        batch_size_val: int = 32,
        batch_size_test: int = 32,
        n_workers: int = 0,
    ):
        """
        Speech Command Lit Data Module initialization

        Parameters
        ----------
        root_folder_raw : str
            root folder directory (folder that contains SpeechCommands folder)
        transform_train : Callable[[str], torch.Tensor]
            Stacked callable transform for training dataset
        transform_valid: Callable[[str], torch.Tensor]
            Stacked callable transform for validation set.
            if None, it will use transform_train
        transform_test: Callable[[str], torch.Tensor]
            Stacked callable  transform for test set.
            If None, it will use transform_test
        speech_command_version: str
            Speech command version to be used
            possible values are "speech_commands_v0.01" or "speech_commands_v0.02"
        batch_size_train: int
            Batch size of training set
        batch_size_val: int
            Batch size of validation set
        batch_size_test: int
            Batch size of testing set
        n_workers: int
            Worker for loading dataloader. By default 1.
        """
        # Well.. I should've created a dataclass for these arguments..
        super().__init__()  # type: ignore
        self.root_folder_raw = root_folder_raw
        self.root_folder_speech = (
            Path(root_folder_raw) / f"SpeechCommands/{speech_command_version}"
        )
        self.transform_train = transform_train
        self.transform_valid = (
            transform_train if transform_valid is None else transform_valid
        )
        self.transform_test = (
            transform_train if transform_test is None else transform_test
        )

        self.speech_train_dataset: Optional[SpeechCommandsDataset] = None
        self.speech_val_dataset: Optional[SpeechCommandsDataset] = None
        self.speech_test_dataset: Optional[SpeechCommandsDataset] = None

        self.batch_size_train = batch_size_train
        self.batch_size_val = batch_size_val
        self.batch_size_test = batch_size_test

        self.n_workers = n_workers

    def prepare_data(self) -> None:
        """
        Data download
        """
        # download
        SpeechCommandsDataset.download_data(self.root_folder_raw)

    def get_label_weight(self) -> NDArray[np.float32]:
        """
        Get label distribution in the training datato do weighting
        for the CrossEntropyLoss
        """
        if self.speech_train_dataset is None:
            self.speech_train_dataset = SpeechCommandsDataset(
                self.root_folder_speech,
                subset="training",
                audio_transformer=self.transform_train,
            )

        labels = [
            self.speech_train_dataset.label_to_idx[label]
            for (_, label) in self.speech_train_dataset.data
        ]

        class_weights = compute_class_weight(  # type: ignore
            class_weight="balanced", classes=np.unique(labels), y=labels  # type: ignore
        ).astype(np.float32)

        # hacky way because there are no _silence_ in the data
        if len(class_weights) == 11:
            class_weights_new = np.zeros(12, dtype=np.float32)
            class_weights_new[0:10] = class_weights[0:10]
            class_weights_new[11] = class_weights[10]
            class_weights = class_weights_new
        return class_weights  # type: ignore

    def setup(self, stage: Optional[str] = None) -> None:

        # Assign train/val datasets for use in dataloaders
        if stage == "fit" or stage is None:
            if self.speech_train_dataset is None:
                self.speech_train_dataset = SpeechCommandsDataset(
                    self.root_folder_speech,
                    subset="training",
                    audio_transformer=self.transform_train,
                )

            self.speech_val_dataset = SpeechCommandsDataset(
                self.root_folder_speech,
                subset="validation",
                audio_transformer=self.transform_valid,
            )

        if stage == "test" or stage is None:
            self.speech_test_dataset = SpeechCommandsDataset(
                self.root_folder_speech,
                subset="testing",
                audio_transformer=self.transform_test,
            )

    def train_dataloader(self) -> DataLoader[Any]:
        if self.speech_train_dataset is None:
            raise ValueError("speech train dataset must be filled")

        persistent_workers = self.n_workers > 1
        return DataLoader(
            self.speech_train_dataset,
            batch_size=self.batch_size_train,
            shuffle=True,
            num_workers=self.n_workers,
            persistent_workers=persistent_workers,
        )

    def val_dataloader(self) -> DataLoader[Any]:
        if self.speech_val_dataset is None:
            raise ValueError("speech val dataset must be filled")
        persistent_workers = self.n_workers > 1
        return DataLoader(
            self.speech_val_dataset,
            batch_size=self.batch_size_val,
            num_workers=self.n_workers,
            persistent_workers=persistent_workers,
        )

    def test_dataloader(self) -> DataLoader[Any]:
        if self.speech_test_dataset is None:
            raise ValueError("speech test dataset must be filled")
        persistent_workers = self.n_workers > 1
        return DataLoader(
            self.speech_test_dataset,
            batch_size=self.batch_size_test,
            num_workers=self.n_workers,
            persistent_workers=persistent_workers,
        )

    def predict_dataloader(self) -> Any:
        raise NotImplementedError
