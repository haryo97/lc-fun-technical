"""
Exploratory Data Analysis utilities Goes Here
"""
# pylint: disable=all


from typing import Optional

import IPython.display as ipd
from IPython.core.display import DisplayObject
from matplotlib import pyplot as plt
from rich.console import Console
from torchaudio import transforms
import librosa
from nlpaug.util.audio.visualizer import AudioVisualizer
import librosa.display as librosa_display

from lc_fun_technical.data import SubsetSC
from .transforms import WavToMelSpectogram


class Audio(DisplayObject):  # type: ignore
    """Create an audio object.
    https://gist.github.com/aisipos/8640905
    When this object is returned by an input cell or passed to the
    display function, it will result in Audio controls being displayed
    in the frontend (only works in the notebook).

    Parameters
    ----------
    data : numpy array, unicode, str or bytes
        Can be a
        * Numpy array containing the desired waveform,
        * String containing the filename
        * Bytestring containing raw PCM data or
        * URL pointing to a file on the web.

        If the array option is used the waveform will be normalized.

        If a filename or url is used the format support will be browser
        dependent.
    url : unicode
        A URL to download the data from.
    filename : unicode
        Path to a local file to load the data from.
    embed : boolean
        Should the image data be embedded using a data URI (True) or be
        loaded using an <img> tag. Set this to True if you want the image
        to be viewable later with no internet connection in the notebook.

        Default is `True`, unless the keyword argument `url` is set, then
        default value is `False`.
    rate : integer
        The sampling rate of the raw data.
        Only required when data parameter is being used as an array
    autoplay : bool
        Set to True if the audio should immediately start playing.
        Default is `False`.

    Examples
    --------

    # Generate a sound
    import numpy as np
    framerate = 44100
    t = np.linspace(0,5,framerate*5)
    data = np.sin(2*np.pi*440*np.sin(10*t**2))
    Audio(data,rate=framerate)

    Audio("http://www.nch.com.au/acm/8k16bitpcm.wav")
    Audio(url="http://media.bradsucks.net/albums/ooi-128/01_-_Brad_Sucks_-_Dropping_out_of_School.mp3")
    Audio(url="http://www.w3schools.com/html/horse.ogg", embed=True)

    Audio('/path/to/sound.wav')
    Audio(filename='/path/to/sound.ogg')

    Audio(b'RAW_WAV_DATA..)
    Audio(data=b'RAW_WAV_DATA..)

    """

    def __init__(  # type: ignore
        self, data=None, filename=None, url=None, embed=None, rate=None, autoplay=False
    ):
        if filename is None and url is None and data is None:
            raise ValueError("No image data found. Expecting filename, url, or data.")
        if embed is False and url is None:
            raise ValueError("No url found. Expecting url when embed=False")

        if url is not None and embed is not True:
            self.embed = False
        else:
            self.embed = True
        self.autoplay = autoplay
        super(Audio, self).__init__(data=data, url=url, filename=filename)

        if self.data is not None and not isinstance(self.data, bytes):  # type: ignore
            self.data = self._make_wav(data, rate)  # type: ignore

    def reload(self):  # type: ignore
        """Reload the raw data from file or URL."""
        import mimetypes

        if self.embed:
            super(Audio, self).reload()

        if self.filename is not None:
            self.mimetype = mimetypes.guess_type(self.filename)[0]
        elif self.url is not None:
            self.mimetype = mimetypes.guess_type(self.url)[0]
        else:
            self.mimetype = "audio/wav"

    def _make_wav(self, data, rate):  # type: ignore
        """Transform a numpy array to a PCM bytestring"""
        import struct
        import wave
        from io import BytesIO

        maxabsvalue = max(map(abs, data))  # type: ignore
        scaled = map(lambda x: int(x / maxabsvalue * 32767), data)
        fp = BytesIO()
        waveobj = wave.open(fp, mode="wb")
        waveobj.setnchannels(1)
        waveobj.setframerate(rate)
        waveobj.setsampwidth(2)
        waveobj.setcomptype("NONE", "NONE")
        waveobj.writeframes(b"".join([struct.pack("<h", x) for x in scaled]))
        val = fp.getvalue()
        waveobj.close()
        return val

    def _data_and_metadata(self):  # type: ignore
        """shortcut for returning metadata with url information, if defined"""
        md = {}
        if self.url:
            md["url"] = self.url
        if md:
            return self.data, md
        else:
            return self.data

    def _repr_html_(self):  # type: ignore
        src = """
                <audio controls="controls" {autoplay}>
                    <source src="{src}" type="{type}" />
                    Your browser does not support the audio element.
                </audio>
              """
        return src.format(
            src=self.src_attr(), type=self.mimetype, autoplay=self.autoplay_attr()
        )

    def src_attr(self):  # type: ignore
        import base64

        if self.embed and (self.data is not None):
            return """data:{type};base64,{base64}""".format(
                type=self.mimetype, base64=base64.b64encode(self.data).decode("ascii")
            )
        elif self.url is not None:
            return self.url
        else:
            return ""

    def autoplay_attr(self):  # type: ignore
        if self.autoplay:
            return 'autoplay="autoplay"'
        else:
            return ""


def notebook_visualize_audio(
    idx: int, dataset: SubsetSC, console: Optional[Console] = None
) -> None:
    """
    Visualize an audio (plot with its audio).
    Only in notebook for exploratory process

    Parameters
    ----------
    idx: int
        Index of file
    console: Optional[Console]
        Rich console handler to provide printing. By Default None
    dataset: SubsetSC
        speech command torchaudio dataset.
    """
    waveform, sample_rate, label, speaker_id, utterance_number = dataset[idx]
    if console is not None:
        console.log(
            dict(
                waveform=waveform,
                sample_rate=sample_rate,
                label=label,
                speaker_id=speaker_id,
                utterance_number=utterance_number,
            )
        )
    ipd.display(ipd.Audio(dataset.get_filename(idx), autoplay=False))
    plt.plot(waveform.t().numpy())
    plt.show()
    if console is not None:
        console.log("Mel Spectogram:")
    transform = transforms.MelSpectrogram()
    mel_spec = transform(waveform)
    plt.figure()
    plt.imshow(mel_spec.log2()[0, :, :].detach().numpy())
    plt.show()


def notebook_visualize_audio_individual(audio_filepath: str, sr: int = 16000) -> None:
    """
    Visualize an audio in notebook

    Parameters
    ----------
    audio_filepath : str
        The audio filepath
    console : Optional[Console], optional
        Console to print if None, not printing, by default None
    sr: int
        audio sample rate
    """
    wav, _ = librosa.load(audio_filepath)
    ipd.display(ipd.Audio(wav, autoplay=False, rate=sr))
    librosa_display.waveshow(y=wav, sr=sr)
    plt.show()
    specto = WavToMelSpectogram()(wav)
    AudioVisualizer.spectrogram("Spectogram", specto)
    plt.show()
