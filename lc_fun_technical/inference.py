"""
Module for inference things
"""


# pylint: disable=invalid-name,unnecessary-comprehension
from transformers import Wav2Vec2Processor
import torch

from lc_fun_technical.modeling.lit_core import LitAudioClassifier
from lc_fun_technical.data import SpeechCommandsDataset
from lc_fun_technical.factory import create_compose_wav2vec2
from .eda import notebook_visualize_audio_individual


def wav2vec_infer_file(
    filename: str,
    lit_model: LitAudioClassifier,
    processor: Wav2Vec2Processor,
    sr: int = 16000,
    visualization: bool = True,
) -> str:
    """
    Infer a file using a wav2vec model

    Parameters
    ----------
    filename: str
        the file of the audio
    ckpt_path: str
        checkpoint path of the audio
    sr: int
        Sample rate
    visualization: bool
        Whether to use visualization or not

    Returns
    -------
    str
        Return the label name
    """
    lit_model.eval()
    if visualization:
        notebook_visualize_audio_individual(filename, sr)
    cps = create_compose_wav2vec2(wav2vec2_processer=processor, sample_rate=sr)
    idx_to_label = {j: i for j, i in enumerate(SpeechCommandsDataset.LABELS)}
    with torch.no_grad():
        inp = cps(filename).unsqueeze(0)
        logits = lit_model(inp)
        label = logits.argmax().item()
        label_name = idx_to_label[label]
    return label_name
