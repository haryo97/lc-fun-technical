"""
Transforms related things goes here
"""

from typing import Any, Callable, List

import numpy as np
from numpy.typing import NDArray
import librosa
import random
import nlpaug.augmenter.audio as naa


class Compose:  # pylint: disable=all
    """
    Compose classes for functions
    """

    def __init__(self, callables: List[Callable[[Any], Any]]):
        """
        Compose callable that can be used to compose function.

        Parameters
        ----------
        callables: List[Callable[Any], Any]
            list of callable that want to be call chained. It must accept
            only an ARGUMENT!
        """
        self.callables = callables

    def __call__(self, x_inp: Any) -> Any:
        out: Any = x_inp
        for callable_func in self.callables:
            out = callable_func(out)
        return out


class AudioLoader:
    """
    Load audio file into a numpy array waveform
    """

    def __init__(self, sample_rate: int = 16000) -> None:
        """
        Load audio file and convert them to waveform

        Parameters
        ----------
        sample_rate: int
            Sample rate of the file
        """
        self.sample_rate = sample_rate

    def __call__(self, x: str) -> NDArray[np.float32]:
        return librosa.load(x, sr=self.sample_rate)[0]  # type: ignore


class WavToMelSpectogram:
    """
    Convert Waveform to Mel Spectogram
    """

    def __init__(
        self,
        sample_rate: int = 16000,
        n_fft: int = 400,
        hop_length: int = 200,
        n_mels: int = 128,
    ):
        self.sample_rate = sample_rate
        self.n_fft = n_fft
        self.hop_length = hop_length
        self.n_mels = n_mels

    def __call__(self, x_data: NDArray[np.float32]) -> NDArray[np.float32]:
        return librosa.feature.melspectrogram(  # type: ignore
            y=x_data,
            sr=self.sample_rate,
            n_fft=self.n_fft,
            hop_length=self.hop_length,
            n_mels=self.n_mels,
        )


class FixAudioLength(object):
    """Padding or Truncate into fixed times (as the dataset says, it's 1 second)"""

    def __init__(self, time: int = 1, sample_rate: int = 16000):
        """
        Initialization

        Parameters
        ----------
        time : int, optional
            time to truncate / pad in second, by default 1
        sample_rate: int
            Sample rate of the audio file
        """
        self.time = time
        self.sample_rate = sample_rate

    def __call__(self, audio_samples: NDArray[np.float32]) -> NDArray[np.float32]:
        length = int(self.time * self.sample_rate)
        if length < len(audio_samples):
            audio_samples = audio_samples[:length]
        elif length > len(audio_samples):
            audio_samples = np.pad(  # type: ignore
                audio_samples, (0, length - len(audio_samples)), "constant"
            )
        return audio_samples


class RandomWavAugmenter:
    """
    Randomize to augment an audio
    """

    def __init__(
        self,
        sample_rate: int = 16000,
        crop_proba: float = 0.3,
        loud_proba: float = 0.3,
        pitch_proba: float = 0.3,
        shift_proba: float = 0,
        speed_proba: float = 0.3,
    ) -> None:
        """
        Augment with probability

        Parameters
        ----------
        sample_rate: int
            The sample rate of the audio, by default 16000
        crop_proba : float, optional
            Crop happen probability, by default 0.3
        loud_proba : float, optional
            Loud augment probability, by default 0.3
        pitch_proba : float, optional
            Pitch augment probability, by default 0.3
        shift_proba : float, optional
            Shift augment probability, by default 0.3
        speed_proba : float, optional
            Speed augment probability, by default 0.3
        """
        self.sample_rate = sample_rate
        self.crop_proba = crop_proba
        self.loud_proba = loud_proba
        self.pitch_proba = pitch_proba
        self.shift_proba = shift_proba
        self.speed_proba = speed_proba
        self.crop_aug = naa.CropAug(sampling_rate=sample_rate)
        self.loud_aug = naa.LoudnessAug()
        self.pitch_aug = naa.PitchAug(sampling_rate=sample_rate, factor=(2, 3))
        # self.shift_aug = naa.ShiftAug(sampling_rate=sample_rate)
        self.speed_aug = naa.SpeedAug()

    def do_randomize(self) -> float:
        """
        Random generator for applying the proba
        """
        return random.random()

    def __call__(self, wav: NDArray[np.float32]) -> NDArray[np.float32]:
        if self.do_randomize() <= self.crop_proba:
            wav = self.crop_aug.augment(wav)
        if self.do_randomize() <= self.loud_proba:
            wav = self.loud_aug.augment(wav)
        if self.do_randomize() <= self.pitch_proba:
            wav = self.pitch_aug.augment(wav)
        # if self.do_randomize() <= self.shift_proba:
        #     wav = self.shift_aug.augment(wav)
        if self.do_randomize() <= self.speed_proba:
            wav = self.speed_aug.augment(wav)
        return wav


# pylint: disable=C0103, R0913
class InjectNoise:
    """Adds noise to an input signal with some probability and some SNR.
    Only adds a single noise file from the list right now.
    FROM: https://github.com/willfrey/audio/blob/master/torchaudio/transforms.py
    """

    def __init__(  # type: ignore
        self, path=None, paths=None, sr=16000, noise_levels=(0, 0.5), probability=0.4
    ):
        if path:
            self.paths = librosa.util.find_files(path)
        else:
            self.paths = paths
        self.sr = sr
        self.noise_levels = noise_levels
        self.probability = probability

    def __call__(self, data):  # type: ignore
        # pylint: disable=E1101
        success = np.random.binomial(1, self.probability)
        if success:
            noise_src, _ = librosa.load(np.random.choice(self.paths), sr=self.sr)
            noise_offset_fraction = np.random.rand()
            noise_level = np.random.uniform(*self.noise_levels)

            noise_dst = np.zeros_like(data)

            src_offset = int(len(noise_src) * noise_offset_fraction)
            src_left = len(noise_src) - src_offset

            dst_offset = 0
            dst_left = len(data)

            while dst_left > 0:
                copy_size = min(dst_left, src_left)
                np.copyto(  # type: ignore
                    noise_dst[dst_offset : dst_offset + copy_size],
                    noise_src[src_offset : src_offset + copy_size],
                )
                if src_left > dst_left:
                    dst_left = 0
                else:
                    dst_left -= copy_size
                    dst_offset += copy_size
                    src_left = len(noise_src)
                    src_offset = 0

            data += noise_level * noise_dst

        return data

    def __getitem__(self, index):
        return self.paths[index]
