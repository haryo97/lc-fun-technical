"""
Data related module
"""
import os
from pathlib import Path
from typing import Callable, List, Optional, Tuple, Union

import torch
from torch import Tensor
from torch.utils.data import Dataset
from torchaudio import transforms
from torchaudio.datasets import SPEECHCOMMANDS


class SpeechCommandsDataset(Dataset):  # type: ignore
    """
    Speech Command Dataset using Librosa.

    Customize the data to align with the label of
    https://www.tensorflow.org/datasets/catalog/speech_commands

    It should contains label in LABELS

    Code are customized from torchaudio
    """

    HASH_DIVIDER = "_nohash_"
    EXCEPT_FOLDER = "_background_noise_"
    LABELS = [
        "down",
        "go",
        "left",
        "no",
        "off",
        "on",
        "right",
        "stop",
        "up",
        "yes",
        "_silence_",
        "_unknown_",
    ]

    UNKNOWN_LABEL = "_unknown_"
    SILENCE_LABEL = "_silence_"

    def __init__(
        self,
        root_folder: Union[str, Path],
        subset: str,
        audio_transformer: Optional[Callable[[str], torch.Tensor]] = None,
    ):
        """
        Initialization

        Parameters
        ----------
        root_folder : Union[str, Path]
            Root folder of the Speech Commands dataset
        subset : str
            subset you want to include.
            choices must be "training", "validation", and "testing"
        audio_transformer : Optional[Callable[[str], torch.Tensor]]
            A callable (or composed callable)
            function that can transform a pathfile into a tensor audio.
        Usage
        -----
        ```py
        SpeechCommandsDataset(
            "data/raw/SpeechCommands/speech_commands_v0.02",
            "training",
            None
        )
        ```
        """
        self.root_folder = root_folder
        self.subset = subset
        self.audio_transformer = audio_transformer
        self.idx_to_label = self.LABELS
        self._prepare_labels()
        self._prepare_data()

    @classmethod
    def download_data(cls, folder: str) -> None:
        """
        Download data directly and put to folder

        Parameters
        ----------
        folder: str
            Folder of the data you want to put in
        """
        SPEECHCOMMANDS(folder, download=True)

    def __getitem__(self, n: int) -> Tuple[Union[str, Tensor], int]:
        """
        Load an audio file and get its file

        Parameters
        ----------
        n : int
            index based on the file

        Returns
        -------
        Tuple[Union[str, Tensor], int]
            _description_
        """
        out_x: Optional[Union[torch.Tensor, str]] = None
        out_x, label_str = self.data[n]
        if self.audio_transformer is not None:
            out_x = self.audio_transformer(out_x)
        return out_x, self.label_to_idx[label_str]

    def __len__(self) -> int:
        return len(self.data)

    def _prepare_walker(self) -> None:
        """
        prepare data walker (traverse to all folders)
        """
        if self.subset == "validation":
            self._walker = self._load_list(self.root_folder, "validation_list.txt")
        elif self.subset == "testing":
            self._walker = self._load_list(self.root_folder, "testing_list.txt")
        elif self.subset == "training":
            excludes = set(
                self._load_list(
                    self.root_folder, "validation_list.txt", "testing_list.txt"
                )
            )
            walker = sorted(str(p) for p in Path(self.root_folder).glob("*/*.wav"))
            self._walker = [
                w
                for w in walker
                if self.HASH_DIVIDER in w
                and self.EXCEPT_FOLDER not in w
                and os.path.normpath(w) not in excludes
            ]
        else:
            raise ValueError("subset must be `validation` `testing` or `training`")

    def _prepare_data(self) -> None:
        """
        Data preparation. Prepare a variable data that contains tuple of
        - audio filepath
        - label (in string)
        """
        # If subset is not validation, testing, or training, throw exception
        self._prepare_walker()
        self.data = [
            (filepath, Path(filepath).parent.name) for filepath in self._walker
        ]

    def _load_list(self, root: Union[str, Path], *filenames: str) -> List[str]:
        """
        Load audio file path
        """
        output = []
        for filename in filenames:
            filepath = os.path.join(root, filename)
            with open(filepath) as fileobj:
                output += [
                    os.path.normpath(os.path.join(root, line.strip()))
                    for line in fileobj
                ]
        return output

    def _prepare_labels(self) -> None:
        all_classes = [
            d
            for d in os.listdir(self.root_folder)
            if os.path.isdir(os.path.join(self.root_folder, d))
            and not d.startswith("_")
            and not d.startswith(".")
        ]
        self.label_to_idx = {j: i for i, j in enumerate(self.idx_to_label)}

        # Put other label into _unknown_
        self.label_to_idx.update(
            {
                j: self.label_to_idx[self.UNKNOWN_LABEL]
                for j in all_classes
                if j not in self.idx_to_label
            }
        )


class SubsetSC(SPEECHCOMMANDS):  # type: ignore
    """
    Modified Class of SPEECHCOMMANDS in torchaudio
    This class is used for EDA.
    """

    def __init__(  # pylint: disable=all
        self,
        root: Union[str, Path],
        url: str = "speech_commands_v0.02",
        folder_in_archive: str = "SpeechCommands",
        download: bool = False,
        subset: Optional[str] = None,
        transform: Optional[Callable[[torch.Tensor], torch.Tensor]] = None,
        is_for_training: bool = False,
    ) -> None:
        """

        Parameters
        ----------
        output_type : str, optional
            output the audio to preferred type
            Choices are "waveform" and "mel"
            "mel": will change the first arguments to become a mel spectogram
            , by default "waveform"
        if_for_training: bool
            Flag to set this object is used for training or not!
            It will affect the __getitem__ behaviour
        """
        super().__init__(root, url, folder_in_archive, download, subset)
        self.transform = transform
        self.is_for_training = is_for_training

        if is_for_training:
            # Do convert label to its index
            ...
        if self.output_type == "mel":
            self.mel_transform = transforms.MelSpectrogram()

    def get_filename(self, idx: int) -> str:
        """
        Get filename from the speech commands

        Parameters
        ----------
        idx: int
            The index of the file you want to retrieve

        Returns
        -------
        str
            Return the filepath you want to retrieve
        """
        # I do this because _walker is a private variable (should be)
        # that cannot be accessed outside of this class
        return self._walker[idx]  # type: ignore

    def __getitem__(self, n: int) -> Tuple[Tensor, int, str, str, int]:
        (
            out_tensor,  # waveform
            sample_rate,
            label,
            speaker_id,
            utterance_number,
        ) = super().__getitem__(n)
        if self.transform is not None:
            out_tensor = self.transform(out_tensor)

        returned_value = None
        if not self.is_for_training:
            returned_value = (
                out_tensor,
                sample_rate,
                label,
                speaker_id,
                utterance_number,
            )
        else:
            raise NotImplementedError("Not implemented yet")
        return returned_value
