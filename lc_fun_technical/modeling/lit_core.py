"""
Lit Core Classification
"""
# pylint: disable=arguments-differ,too-many-ancestors

from dataclasses import dataclass
from typing import Optional, Tuple

import numpy as np
import pytorch_lightning as pl
import torch
import torchmetrics
from numpy.typing import NDArray
from rich.console import Console
from torch import nn
from torch.nn import functional as F
from torchvision.models import resnet34, resnext50_32x4d
from transformers import AutoConfig

from lc_fun_technical.modeling.wav2vec_clf import Wav2Vec2ForSpeechClassification

console = Console()


@dataclass
class LitAudioClassifierArgs:
    """
    Argument for LitAudioClassifier

    Parameters
    ----------
    learning_rate: float
        The learning rate of the model
    backbone_choice: str
        Backbone choice of the model
    num_classes: int
        Number of class to be trained
    pretrained: bool
        Whether the model is pretrained or not
    """

    learning_rate: float = 1e-3
    backbone_choice: str = "resnet34"
    num_classes: int = 12
    pretrained: bool = False
    class_weight: Optional[NDArray[np.float32]] = None
    pretrained_name: Optional[str] = None


class LitAudioClassifier(  # pylint: disable=too-many-instance-attributes
    pl.LightningModule
):
    """
    Audio Classification using Pytorch Lightning!
    """

    def __init__(self, lit_args: LitAudioClassifierArgs):
        super().__init__()
        self.lit_args = lit_args
        self.learning_rate = lit_args.learning_rate
        self.backbone_choice = lit_args.backbone_choice
        self.backbone_out_logit = False
        self.linear_to_out: Optional[torch.nn.Module] = None
        self.train_acc = torchmetrics.Accuracy()
        self.valid_acc = torchmetrics.Accuracy()
        self._class_weight: Optional[torch.Tensor] = None
        self._init_model()

    def _init_model(self) -> None:
        if self.backbone_choice == "resnet34":
            self.backbone = resnet34(self.lit_args.pretrained)
            self.backbone.fc = nn.Linear(512, self.lit_args.num_classes)
            self.backbone.conv1 = nn.Conv2d(
                1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False
            )
            self.backbone_out_logit = True
        elif self.backbone_choice == "resnext50_32x4d":
            self.backbone = resnext50_32x4d(self.lit_args.pretrained)
            self.backbone.fc = nn.Linear(2048, self.lit_args.num_classes)
            self.backbone.conv1 = nn.Conv2d(
                1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False
            )
            self.backbone_out_logit = True
        elif self.backbone_choice == "wav2vec2":
            if not self.lit_args.pretrained:
                raise NotImplementedError("wav2vec2 must use pretrained")
            # config
            config = AutoConfig.from_pretrained(
                self.lit_args.pretrained_name,
                num_labels=self.lit_args.num_classes,
                label2id={
                    label: i
                    for i, label in enumerate(list(range(self.lit_args.num_classes)))
                },
                id2label={
                    i: label
                    for i, label in enumerate(  # pylint: disable=all
                        list(range(self.lit_args.num_classes))
                    )
                },
                finetuning_task="wav2vec2_clf",
            )
            setattr(config, "pooling_mode", "mean")
            self.backbone = Wav2Vec2ForSpeechClassification.from_pretrained(
                self.lit_args.pretrained_name,
                config=config,
            )
            self.backbone_out_logit = True
        else:
            raise NotImplementedError(f"{self.backbone_choice} is not implemented yet!")

    def forward(self, x: torch.Tensor):  # type: ignore
        logits: Optional[torch.Tensor] = None
        if self.backbone_out_logit:
            logits = self.backbone(x)
            if self.backbone_choice == "wav2vec2":
                # output of wav2vec2 is in SpeechClassifierOutput
                # need to access its logits
                logits = logits.logits
        else:
            out_backbone = self.backbone(x)
            if self.linear_to_out is not None:
                logits = self.linear_to_out(  # pylint: disable=not-callable
                    out_backbone
                )
            else:
                raise Exception("Something wrong with the brain of the programmer")
        return logits

    def training_step(  # type: ignore
        self,
        batch: Tuple[torch.Tensor, torch.Tensor],
        batch_idx: int,  # pylint: disable=unused-argument
    ):
        x_data, y_data = batch
        y_hat = self(x_data)
        self.train_acc(y_hat, y_data)

        if self._class_weight is None and self.lit_args.class_weight is not None:
            console.print("[red]Assign Weights![/red]")
            self._class_weight = torch.Tensor(self.lit_args.class_weight).to(
                self.device
            )
        loss = F.cross_entropy(y_hat, y_data, weight=self._class_weight)
        self.log("train_loss", loss, on_epoch=True)
        self.log(
            "train_acc", self.train_acc, on_step=True, on_epoch=False, prog_bar=True
        )
        return loss

    def validation_step(  # type: ignore
        self,
        batch: Tuple[torch.Tensor, torch.Tensor],
        batch_idx: int,  # pylint: disable=unused-argument
    ):
        x_data, y_data = batch
        y_hat = self(x_data)
        self.valid_acc(y_hat, y_data)
        loss = F.cross_entropy(y_hat, y_data)
        self.log("valid_loss", loss, on_step=False, on_epoch=True, prog_bar=True)
        self.log(
            "valid_acc", self.valid_acc, on_step=False, on_epoch=True, prog_bar=True
        )

    def test_step(  # type: ignore
        self, batch: Tuple[torch.Tensor, torch.Tensor], batch_idx: int
    ):  # pylint: disable=unused-argument
        x_data, y_data = batch
        y_hat = self(x_data)
        self.train_acc(y_hat, y_data)
        loss = F.cross_entropy(y_hat, y_data)
        self.log("test_loss", loss, on_epoch=True)
        self.log("test_acc", self.train_acc, on_step=False, on_epoch=True)
        return loss

    def predict_dataloader(self):  # type: ignore
        raise NotImplementedError

    def train_dataloader(self):  # type: ignore
        raise NotImplementedError

    def configure_optimizers(self):  # type: ignore
        return torch.optim.Adam(self.parameters(), lr=self.learning_rate)
