"""
Factory function goes here.
It consists of obj creation of composing a function
to transform audio file to be ready for training or inference
"""
from functools import partial
from glob import glob

import numpy as np
import torch
from transformers import Wav2Vec2Processor

from .transforms import (
    AudioLoader,
    Compose,
    FixAudioLength,
    InjectNoise,
    RandomWavAugmenter,
    WavToMelSpectogram,
)


def create_compose_wav2vec2_aug_noise(
    wav2vec2_processer: Wav2Vec2Processor, sample_rate: int = 16000
) -> Compose:
    """
    Create a transformation function for wav2vec2 by injecting noise
    and do augmentation.

    Just wondering if wav2vec2 is prone to noise

    Parameters
    ----------
    sample_rate: int, optional
        sample rate of the audio, by default 16000
    """
    # REFACTOR LATER! NO HARDCODE!
    # path for background noise
    glob_path = glob(
        "data/raw/SpeechCommands/speech_commands_v0.02/_background_noise_/*.wav"
    )

    def process_wav(wav):
        return wav2vec2_processer(wav, sampling_rate=sample_rate)["input_values"][0]

    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            RandomWavAugmenter(sample_rate=sample_rate),
            InjectNoise(paths=glob_path, sr=sample_rate),  # type: ignore
            FixAudioLength(sample_rate=sample_rate),
            process_wav,
            torch.Tensor,
        ]
    )
    return cps


def create_compose_wav2vec2(
    wav2vec2_processer: Wav2Vec2Processor, sample_rate: int = 16000
) -> Compose:
    """
    Create a transformation function for wav2vec2

    Parameters
    ----------
    sample_rate: int, optional
        sample rate of the audio, by default 16000
    """

    def process_wav(wav):
        return wav2vec2_processer(wav, sampling_rate=sample_rate)["input_values"][0]

    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            FixAudioLength(sample_rate=sample_rate),
            process_wav,
            torch.Tensor,
        ]
    )
    return cps


def create_compose_training_default(sample_rate: int = 16000) -> Compose:
    """
    Create a transformation function compose for default
    scenario

    Parameters
    ----------
    sample_rate : int, optional
        samplerate of the audio, by default 16000
    """
    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            FixAudioLength(sample_rate=sample_rate),
            WavToMelSpectogram(sample_rate=sample_rate),
            partial(np.expand_dims, axis=0),
            torch.Tensor,
        ]
    )
    return cps


def create_compose_training_with_aug(sample_rate: int = 16000) -> Compose:
    """
    Create a transformation function compose for data with augmentation
    scenario

    Parameters
    ----------
    sample_rate : int, optional
        samplerate of the audio, by default 16000
    """

    # REFACTOR LATER! NO HARDCODE!
    # path for background noise
    glob_path = glob(
        "data/raw/SpeechCommands/speech_commands_v0.02/_background_noise_/*.wav"
    )
    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            RandomWavAugmenter(sample_rate=sample_rate),
            InjectNoise(paths=glob_path, sr=16000),  # type: ignore
            FixAudioLength(sample_rate=sample_rate),
            WavToMelSpectogram(sample_rate=sample_rate),
            partial(np.expand_dims, axis=0),
            torch.Tensor,
        ]
    )

    return cps


def create_compose_training_with_aug_v2(sample_rate: int = 16000) -> Compose:
    """
    Create a transformation function compose for data with augmentation
    scenario

    Parameters
    ----------
    sample_rate : int, optional
        samplerate of the audio, by default 16000
    """

    # REFACTOR LATER! NO HARDCODE!
    # path for background noise
    glob_path = glob(
        "data/raw/SpeechCommands/speech_commands_v0.02/_background_noise_/*.wav"
    )
    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            RandomWavAugmenter(sample_rate=sample_rate),
            InjectNoise(paths=glob_path, sr=16000),  # type: ignore
            FixAudioLength(sample_rate=sample_rate),
            WavToMelSpectogram(
                sample_rate=sample_rate, n_fft=2048, hop_length=512, n_mels=32
            ),
            partial(np.expand_dims, axis=0),
            torch.Tensor,
        ]
    )

    return cps


def create_compose_training_infer_aug_v2(sample_rate: int = 16000) -> Compose:
    """
    Create a transformation function compose for data with augmentation
    scenario

    Parameters
    ----------
    sample_rate : int, optional
        samplerate of the audio, by default 16000
    """

    # REFACTOR LATER! NO HARDCODE!
    # path for background noise
    # glob_path = glob(
    #     "data/raw/SpeechCommands/speech_commands_v0.02/_background_noise_/*.wav"
    # )
    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            FixAudioLength(sample_rate=sample_rate),
            WavToMelSpectogram(
                sample_rate=sample_rate, n_fft=2048, hop_length=512, n_mels=32
            ),
            partial(np.expand_dims, axis=0),
            torch.Tensor,
        ]
    )

    return cps


def create_compose_test(sample_rate: int = 16000) -> Compose:
    """
    Testing!

    Parameters
    ----------
    sample_rate : int, optional
        samplerate of the audio, by default 16000
    """

    # REFACTOR LATER! NO HARDCODE!
    # path for background noise
    # glob_path = glob(
    #     "data/raw/SpeechCommands/speech_commands_v0.02/_background_noise_/*.wav"
    # )
    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            #             RandomWavAugmenter(sample_rate=sample_rate),
            #             InjectNoise(paths=glob_path, sr=sample_rate),  # type: ignore
            FixAudioLength(sample_rate=sample_rate),
            WavToMelSpectogram(
                sample_rate=sample_rate, n_fft=2048, hop_length=512, n_mels=120
            ),
            partial(np.expand_dims, axis=0),
            torch.Tensor,
        ]
    )

    return cps


def create_compose_test_2(sample_rate: int = 16000) -> Compose:
    """
    Testing_2

    Parameters
    ----------
    sample_rate : int, optional
        samplerate of the audio, by default 16000
    """

    # REFACTOR LATER! NO HARDCODE!
    # path for background noise
    # glob_path = glob(
    #     "data/raw/SpeechCommands/speech_commands_v0.02/_background_noise_/*.wav"
    # )
    cps = Compose(
        [
            AudioLoader(sample_rate=sample_rate),
            RandomWavAugmenter(sample_rate=sample_rate),
            #             InjectNoise(paths=glob_path, sr=sample_rate),  # type: ignore
            FixAudioLength(sample_rate=sample_rate),
            WavToMelSpectogram(
                sample_rate=sample_rate, n_fft=2048, hop_length=512, n_mels=120
            ),
            partial(np.expand_dims, axis=0),
            torch.Tensor,
        ]
    )

    return cps


MAPPER_COMPOSE_FUNC = {
    "wav2vec_aug_noise_cps": create_compose_wav2vec2_aug_noise,
    "wav2vec2_cps": create_compose_wav2vec2,
    "default_cps": create_compose_training_default,
    "aug_cps": create_compose_training_with_aug,
    "aug_cps_2": create_compose_training_with_aug_v2,
    "default_cps_2": create_compose_training_infer_aug_v2,
    "test": create_compose_test,
    "test2": create_compose_test_2,
}
