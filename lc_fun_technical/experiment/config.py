"""
Experiment Config Goes Here!
"""

from dataclasses import dataclass, field
from typing import Any, Dict, Optional


@dataclass
class GeneralConfig:
    """
    General config of the experiment
    """

    seed: int = 1234


@dataclass
class DataConfig:
    """
    Data config of the experiment

    See pytorch lightning callback
    for each of them
    """

    sample_rate: int
    lit_data_args: Dict[str, Any]


@dataclass
class TrainingProcessConfig:
    """
    Trainer config

    See pytorch lightning callback for
    each of them

    Parameters
    ----------
    use_balancing_feature: bool
        whether to use data imbalance problem solver
        or not
    """

    use_balancing_feature: bool = False
    early_stopping_args: Optional[Dict[str, Any]] = None
    model_checkpoint_args: Optional[Dict[str, Any]] = None
    wandb_logger_args: Optional[Dict[str, Any]] = None
    trainer_args: Dict[str, Any] = field(default_factory=dict)
