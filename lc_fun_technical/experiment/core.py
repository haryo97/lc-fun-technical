"""
Core of experiment runner!
"""
from typing import Any, Dict, List
import warnings

import pytorch_lightning as pl
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from pytorch_lightning.loggers import WandbLogger
from rich.console import Console
from transformers import Wav2Vec2Processor
from lc_fun_technical.factory import MAPPER_COMPOSE_FUNC
from lc_fun_technical.modeling.lit_core import (
    LitAudioClassifier,
    LitAudioClassifierArgs,
)

from lc_fun_technical.pl_dataprocesser import SpeechCommandLitDataModule
from .config import DataConfig, GeneralConfig, TrainingProcessConfig


console = Console()


class MainExperiment:
    """
    Experiment Runner Class
    """

    def __init__(
        self, experiment_config: Dict[str, Any], gpus: List[int], is_test: bool = False
    ) -> None:
        """
        Experimen Runner

        Parameters
        ----------
        experiment_config : Dict[str, Any]
            Experiment config that should follow configuration in config.py
        gpus : List[int]
            gpus that will be used. if empty list, it will use cpu
        is_test : bool, optional
            whether it's a test (pytest) or not, by default False
        """
        warnings.filterwarnings("ignore")
        data_cfg = experiment_config.pop("data")
        modeling_cfg = experiment_config.pop("modeling")
        training_process_cfg = experiment_config.pop("training_process_args")

        self.data_config = DataConfig(**data_cfg)
        self.modeling_config = LitAudioClassifierArgs(**modeling_cfg)
        self.training_process_config = TrainingProcessConfig(**training_process_cfg)
        self.general_config = GeneralConfig(**experiment_config)
        self.is_test = is_test
        # Adapt gpus
        if len(gpus) == 0:
            self.training_process_config.trainer_args["gpus"] = None
        else:
            self.training_process_config.trainer_args["gpus"] = gpus
        if len(gpus) > 1:
            self.training_process_config.trainer_args["strategy"] = "ddp"

    def produce_lit_datamodule(self) -> SpeechCommandLitDataModule:
        """
        Lit datamodule wrangling goes here!
        """
        data_config_dict = self.data_config.__dict__
        console.print(f"Producing Lit Data Module with {data_config_dict}")

        lit_datamodule_args = self.data_config.lit_data_args
        for key in ["transform_train", "transform_valid", "transform_test"]:
            if key in lit_datamodule_args:
                lit_datamodule_args[key] = MAPPER_COMPOSE_FUNC[
                    lit_datamodule_args[key]
                ](sample_rate=self.data_config.sample_rate)

        lit_datamodule = SpeechCommandLitDataModule(**lit_datamodule_args)
        return lit_datamodule

    def produce_lit_model(self) -> LitAudioClassifier:
        """
        Produce lit model here!
        """
        console.print(f"Producing Model with {self.modeling_config.__dict__}!")
        lit_model = LitAudioClassifier(self.modeling_config)
        return lit_model

    def prepare_and_run_training(
        self,
        lit_datamodule: SpeechCommandLitDataModule,
        lit_model: LitAudioClassifier,
    ) -> None:
        """
        Prepare callback, loggers, and train!

        Parameters
        ---------
        lit_datamodule: SpeechCommandLitDataModule
            Lit command data module pl
        lit_model: LitAudioClassifier
            Lit model ready to be used!
        """
        # Assign data imbalance solver to weight
        if self.training_process_config.use_balancing_feature:
            lit_model.lit_args.class_weight = lit_datamodule.get_label_weight()
        callbacks: List[Any] = []
        logger = None
        if self.training_process_config.early_stopping_args is not None:
            callbacks.append(
                EarlyStopping(**self.training_process_config.early_stopping_args)
            )
        if self.training_process_config.model_checkpoint_args is not None:
            callbacks.append(
                ModelCheckpoint(**self.training_process_config.model_checkpoint_args)
            )
        if self.training_process_config.wandb_logger_args is not None:
            logger = WandbLogger(**self.training_process_config.wandb_logger_args)
        console.print(
            f"Producing Model with {self.training_process_config.trainer_args}!"
        )

        trainer = Trainer(
            callbacks=callbacks,
            logger=logger,
            **self.training_process_config.trainer_args,
        )
        if "auto_lr_find" in self.training_process_config.trainer_args:
            console.print("[red]lr[/red] find!")
            trainer.tune(lit_model, datamodule=lit_datamodule)

        trainer.fit(lit_model, datamodule=lit_datamodule)
        if not self.is_test:
            trainer.test(lit_model, lit_datamodule, ckpt_path="best")

    def run(self) -> None:
        """
        Run the experiment end to end
        """
        pl.seed_everything(self.general_config.seed, workers=True)
        lit_datamodule = self.produce_lit_datamodule()
        lit_model = self.produce_lit_model()
        self.prepare_and_run_training(lit_datamodule, lit_model)


class Wav2VecExperiment(MainExperiment):
    """
    Experiment Runner Class for wav2vec model
    """

    def produce_lit_datamodule(self) -> SpeechCommandLitDataModule:
        """
        Lit datamodule wrangling goes here!
        """
        data_config_dict = self.data_config.__dict__
        console.print(f"Producing Lit Data Module with {data_config_dict}")
        processer = Wav2Vec2Processor.from_pretrained(
            self.modeling_config.pretrained_name
        )
        lit_datamodule_args = self.data_config.lit_data_args
        for key in ["transform_train", "transform_valid", "transform_test"]:
            if key in lit_datamodule_args:
                lit_datamodule_args[key] = MAPPER_COMPOSE_FUNC[
                    lit_datamodule_args[key]
                ](
                    sample_rate=self.data_config.sample_rate,
                    wav2vec2_processer=processer,
                )

        lit_datamodule = SpeechCommandLitDataModule(**lit_datamodule_args)
        return lit_datamodule

    def produce_lit_model(self) -> LitAudioClassifier:
        """
        Produce lit model here!
        """
        console.print(f"Producing Model with {self.modeling_config.__dict__}!")
        lit_model = LitAudioClassifier(self.modeling_config)
        lit_model.backbone.freeze_feature_extractor()
        return lit_model

    def run(self) -> None:
        """
        Run the experiment end to end
        """
        pl.seed_everything(self.general_config.seed, workers=True)
        lit_datamodule = self.produce_lit_datamodule()
        lit_model = self.produce_lit_model()
        self.prepare_and_run_training(lit_datamodule, lit_model)
