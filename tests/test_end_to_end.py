"""
TEST!
"""

from lc_fun_technical.experiment.core import MainExperiment, Wav2VecExperiment


def test_end_to_end_main_training():
    """
    Test the capability of end to end training in `main`
    experiment
    only check whether it runs error or not
    """
    cfg = {
        "seed": 1234,
        "data": {
            "sample_rate": 16000,
            "lit_data_args": {
                "root_folder_raw": "data/raw",
                "transform_train": "default_cps",
                "batch_size_train": 2,
                "batch_size_val": 2,
                "batch_size_test": 2,
            },
        },
        "modeling": {
            "learning_rate": 0.001,
            "backbone_choice": "resnet34",
            "num_classes": 12,
            "pretrained": False,
        },
        "training_process_args": {
            "early_stopping_args": {
                "monitor": "valid_loss",
                "patience": 5,
                "mode": "min",
            },
            "model_checkpoint_args": {
                "dirpath": "outputs/model/test_main/",
                "filename": "{epoch}-{valid_loss:.4f}-{valid_acc:.2f}",
                "monitor": "valid_loss",
                "save_last": True,
                "save_top_k": 1,
                "every_n_epochs": 1,
            },
            "trainer_args": {"fast_dev_run": True},
        },
    }
    exp = MainExperiment(cfg, gpus=[], is_test=True)
    exp.run()


def test_end_to_end_wav2vec_training():
    """
    Test the capability of end to end training in `wav2vec`
    experiment
    only check whether it runs error or not
    """
    cfg = {
        "seed": 1234,
        "data": {
            "sample_rate": 16000,
            "lit_data_args": {
                "root_folder_raw": "data/raw",
                "transform_train": "wav2vec2_cps",
                "batch_size_train": 2,
                "batch_size_val": 2,
                "batch_size_test": 2,
            },
        },
        "modeling": {
            "learning_rate": 0.001,
            "backbone_choice": "wav2vec2",
            "num_classes": 12,
            "pretrained": True,
            "pretrained_name": "facebook/wav2vec2-base-960h"
        },
        "training_process_args": {
            "early_stopping_args": {
                "monitor": "valid_loss",
                "patience": 5,
                "mode": "min",
            },
            "model_checkpoint_args": {
                "dirpath": "outputs/model/test_wav2vec/",
                "filename": "{epoch}-{valid_loss:.4f}-{valid_acc:.2f}",
                "monitor": "valid_loss",
                "save_last": True,
                "save_top_k": 1,
                "every_n_epochs": 1,
            },
            "trainer_args": {"fast_dev_run": True},
        },
    }
    exp = Wav2VecExperiment(cfg, gpus=[], is_test=True)
    exp.run()
